var startExperience = require('thesProject/achievementHandler');

function handleAchievement(event) {
	echo(event.player, event.getAchievement()); // Used for getting of achievement names
	startExperience.achievementMenu(event);
}

events.playerAchievementAwarded(handleAchievement);