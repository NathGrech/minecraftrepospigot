var input = require('input');
var noOfWood = 0;
var noOfWoodenPlanks = 0;
var noOfCobblestone = 0;
var noOfSticks = 0;
var redIndex = 4;
var greenIndex = 2;
var orangeIndex = 6;
var noOfSticks = 0;
var noOfWheat = 0;
var pendingProgress = '';
var sessionPlayer = null;
var lastUpdatedPlanks = 0;
var lastUpdatedSticks = 0;
var lastUpdatedCobblestone = 0;
var lastUpdatedWheat = 0;
var alreadyEchoed = 0;
var initialInstruction = 0;
var pathsToTake = ['Farmer', 'Warrior', 'Miner'];
var pathBeingFollowed = '';



function achievementMenu(event) {
	var achName = event.getAchievement();
	var usrName = event.player.name;
	sessionPlayer = event.player;
	events.playerMove(checkUpdatedTips);
	
	if(achName == 'OPEN_INVENTORY')
	{
		echo(sessionPlayer, '\xa7' + greenIndex.toString(16) + 'Congratulations on your first achievement! Now, find a tree and punch the block until it is destroyed. When this happens, make sure to pick up the wood it drops.');
	}
	else if(achName == 'MINE_WOOD')
	{
		echo(sessionPlayer, '\xa7' + greenIndex.toString(16) + 'Proceed to put the wood block into your inventory crafting section and turn the wood block into 4 woodplanks.');
		pendingProgress = 'Get planks for crafting table';
	}
	else if(achName == 'BUILD_WORKBENCH')
	{
		alreadyEchoed = 0;
		pendingProgress = 'Choosing class';
		input(sessionPlayer, '\xa7' + greenIndex.toString(16) + 'Congratulations. Which path do you want to take? Farmer(a), Warrior(b) or Miner(c)', firstDecision);
	}
	else if(achName == 'BUILD_HOE')
	{
		initialInstruction = 0;
		pendingProgress = 'farming stage2';
		echo(sessionPlayer, '\xa7' + greenIndex.toString(16) + 'Destroy grass until seeds are dropped then right click using your hoe on a dirt block close to water. When this is done, place the seeds using right click in the dirt block which is hoed and wait until the wheat is ready to be harvested. Afterwards, destroy the wheat to place it in the inventory.');
	}
	else if(achName == 'BUILD_SWORD')
	{
		initialInstruction = 0;
		pendingProgress = 'warrior stage2';
		echo(sessionPlayer, '\xa7' + greenIndex.toString(16) + 'Use your sword to kill cows until they drop leather then pick it up.');
	}
	else if(achName == 'BUILD_PICKAXE')
	{
		initialInstruction = 0;
		pendingProgress = 'mining stage2';
	}
	else if(achName == 'BUILD_FURNACE')
	{
		echo(sessionPlayer, '\xa7' + redIndex.toString(16) + 'This marks the end of the experience so far. I hope you enjoyed it and please make sure to answer the survey which will be provided to you! Thanks a lot! :)');
		initialInstruction = 0;
	}
	else if(achName == 'KILL_COW')
	{
		echo(sessionPlayer, '\xa7' + redIndex.toString(16) + 'This marks the end of the experience so far. I hope you enjoyed it and please make sure to answer the survey which will be provided to you! Thanks a lot! :)');
		initialInstruction = 0;
	}
	else if(achName == 'MAKE_BREAD')
	{
		echo(sessionPlayer, '\xa7' + redIndex.toString(16) + 'This marks the end of the experience so far. I hope you enjoyed it and please make sure to answer the survey which will be provided to you! Thanks a lot! :)');
		initialInstruction = 0;
	}
	
	if(pendingProgress != '')
	{
		updateTips(event.player);
	}
};

function firstDecision(decision, player)
{
	decision = decision.toLowerCase();
	initialInstruction = 0;
	
	switch(decision)
	{
		case 'a':
			echo(player, '\xa7' + orangeIndex.toString(16) + 'Wise! You have chosen to be a farmer!');
			pendingProgress = 'farming stage1';
			break;
		case 'b':
			echo(player, '\xa7' + orangeIndex.toString(16) + 'Brave! You have chosen to be a warrior!');
			pendingProgress = 'warrior stage1';
			break;
		case 'c':
			echo(player, '\xa7' + orangeIndex.toString(16) + 'Ooh! You have chosen to be a miner!');
			pendingProgress = 'mining stage1';
			break;
		default:
			repeat();
	}
	
	updateTips(event.player);
}

function updateTips(player)
{
	//echo(player, pendingProgress);
	//echo(player, noOfWoodenPlanks);
	switch(pendingProgress)
	{
		case 'Get planks for crafting table':
			if(noOfWoodenPlanks >= 4)
			{
				if(alreadyEchoed == 0)
				{
					echo(player, "\xa7" + orangeIndex.toString(16) + 'Put 4 wooden planks in the crafting slot to create a crafting table.');
					alreadyEchoed = 1;
				}
			}
			else
			{
				alreadyEchoed = 0;
				if(lastUpdatedPlanks != noOfWoodenPlanks || initialInstruction == 0)
				{
					lastUpdatedPlanks = noOfWoodenPlanks;
					echo(player, "\xa7" + redIndex.toString(16) + pendingProgress + ' ' + noOfWoodenPlanks + '/4 planks');
					initialInstruction = 1;
				}
			}
			break;
		case 'farming stage1':
			if(noOfWoodenPlanks >= 2 && noOfSticks >= 3)
			{
				if(alreadyEchoed == 0)
				{
					echo(player, "\xa7" + orangeIndex.toString(16) + 'Put 2 sticks horizontal starting from the bottom and 2 wooden planks in the top left and top middle slot of the crafting table');
					alreadyEchoed = 1;
				}
			}
			else
			{
				alreadyEchoed = 0;
				if(lastUpdatedPlanks != noOfWoodenPlanks || lastUpdatedSticks != noOfSticks || initialInstruction == 0)
				{
						lastUpdatedPlanks = noOfWoodenPlanks;
						lastUpdatedSticks = noOfSticks;
						echo(player, "\xa7" + redIndex.toString(16) + 'Gather at least 2 sticks and 2 wooden planks ' + noOfSticks + '/2 sticks ' + noOfWoodenPlanks + '/2 planks'); 
						initialInstruction = 1;
				}
			}
			break;
		case 'warrior stage1':
			if(noOfWoodenPlanks >= 2 && noOfSticks >= 1)
			{
				if(alreadyEchoed == 0)
				{
					echo(player, "\xa7" + orangeIndex.toString(16) + 'Put 1 stick in the bottom middle slot and 2 wooden planks in the above slots of the crafting table');
					alreadyEchoed = 1;
				}
			}
			else
			{
				alreadyEchoed = 0;
				if(lastUpdatedPlanks != noOfWoodenPlanks || lastUpdatedSticks != noOfSticks || initialInstruction == 0)
				{
					lastUpdatedPlanks = noOfWoodenPlanks;
					lastUpdatedSticks = noOfSticks;
					echo(player, "\xa7" + redIndex.toString(16) + 'Gather at least 1 stick and 2 wooden planks ' + noOfSticks + '/1 sticks ' + noOfWoodenPlanks + '/2 planks');
					initialInstruction = 1;
				}
			}
			break;
		case 'mining stage1':
			if(noOfWoodenPlanks >= 3 && noOfSticks >= 2)
			{
				if(alreadyEchoed == 0)
				{
					echo(player, "\xa7" + orangeIndex.toString(16) + 'Put 1 stick in the bottom middle slot and 1 above it and 3 wooden planks in the top row of the crafting table');
					alreadyEchoed = 1;
				}
			}
			else
			{
				alreadyEchoed = 0;
				if(lastUpdatedPlanks != noOfWoodenPlanks || lastUpdatedSticks != noOfSticks || initialInstruction == 0)
				{
					lastUpdatedPlanks = noOfWoodenPlanks;
					lastUpdatedSticks = noOfSticks;
					echo(player, "\xa7" + redIndex.toString(16) + 'Gather at least 2 sticks and 3 wooden planks ' + noOfSticks + '/2 sticks ' + noOfWoodenPlanks + '/3 planks');
					initialInstruction = 1;
				}
			}
			break;
		case 'mining stage2':
			if(noOfCobblestone >= 8)
			{
				if(alreadyEchoed == 0)
				{
					echo(player, "\xa7" + orangeIndex.toString(16) + 'Place cobblestone in each slot apart from the middle slot of the crafting table to create a furnace');
					alreadyEchoed = 1;
				}
			}
			else
			{
				alreadyEchoed = 0;
				if(lastUpdatedCobblestone != noOfCobblestone || initialInstruction == 0)
				{
					lastUpdatedCobblestone = noOfCobblestone;
					echo(player, "\xa7" + redIndex.toString(16) + 'Mine stone using the pickaxe and gather 8 cobblestone ' + noOfCobblestone + '/8 cobblestone');
					initialInstruction = 1;
				}
			}
			break;
		case 'farming stage2':
			if(noOfWheat >= 3)
			{
				if(alreadyEchoed == 0)
				{
					echo(player, "\xa7" + orangeIndex.toString(16) + 'Fill the middle row of the crafting table with wheat');
					alreadyEchoed = 1;
				}
			}
			else
			{
				alreadyEchoed = 0;
				if(lastUpdatedWheat != noOfWheat || initialInstruction == 0)
				{
					lastUpdatedWheat = noOfWheat;
					echo(player, "\xa7" + redIndex.toString(16) + 'Gather 3 wheat ' + noOfWheat + '/3 wheat');
					initialInstruction = 1;
				}
			}
			break;
		default:
				
	}
}

function checkInventory(player)
{
	var contents = player.getInventory().getContents();
	var len = contents.length;
	var foundWood = 0;
	var foundCobblestone = 0;
	var foundPlanks = 0;
	var foundBench = 0;
	var foundSticks = 0;
	var foundWheat = 0;
	
	updatedInventoryFlag = 0;
	
	for(var i = 0;i < len; i++)
	{
		var item = contents[i];
		
		//echo(player, item);
		
		if(item != null /*&& !item.getType().equals(Material.AIR)*/)
		{
			if(item.getType() == 'LOG')
			{
				noOfWood = item.getAmount();
				foundWood = 1;
			}
			else if(item.getType() == 'WOOD')
			{
				noOfWoodenPlanks = item.getAmount();
				foundPlanks = 1;
			}
			else if(item.getType() == 'STICK')
			{
				noOfSticks = item.getAmount();
				foundSticks = 1;
			}
			else if(item.getType() == 'COBBLESTONE')
			{
				noOfCobblestone = item.getAmount();
				foundCobblestone = 1;
			}
			else if(item.getType() == 'WHEAT')
			{
				noOfWheat = item.getAmount();
				foundWheat = 1;
			}
		}
	}
	
	if(foundWood == 0 && noOfWood != 0)
	{
		noOfWood = 0;
	}
	
	if(foundPlanks == 0 && noOfWoodenPlanks != 0)
	{
		noOfWoodenPlanks = 0;
	}
	
	if(foundSticks == 0 && noOfSticks != 0)
	{
		noOfSticks = 0;
	}
	
	if(foundCobblestone == 0 && noOfCobblestone != 0)
	{
		noOfCobblestone = 0;
	}
	
	if(foundWheat == 0 && noOfWheat != 0)
	{
		noOfWheat = 0;
	}
}

function checkUpdatedTips(event)
{
	checkInventory(event.player);
	updateTips(event.player);
}

exports.achievementMenu = achievementMenu;
events.playerMove(checkUpdatedTips);